var myApp = angular.module('myApp', []);

myApp.controller('mainController', ['$scope', '$filter', function($scope, $filter) {
    
    $scope.handle = '';
    
    $scope.lowercasehandle = function() {
        return $filter('lowercase')($scope.handle);
    };
    
    $scope.characters = 5;
    $scope.rules = [
      
        { rulename: "Must be 5 characters" },
        { rulename: "Must not be used elsewhere" },
        { rulename: "Must be cool" }
        
    ];
    
    console.log($scope.rules);
    
    // var rulesrequest = new HttpRequest();
    // rulesrequest.onreadystatechange = function () {
    //     $scope.$apply(function () {
    //         if (rulesrequest.readyState == 4 && rulesrequest.status == 200) {
    //             $scope.rules = JSON.parse(rulesrequest.responseText);
    //         }
    //     });
    // }
    // rulesrequest.open("GET", "http://localhost:5500/api", true);
    // rulesrequest.send();
    
}]);
