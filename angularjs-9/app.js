var myApp = angular.module('myApp', ['ngRoute']);

myApp.config(function ($routeProvider) {
    
    $routeProvider
    
    .when('/', {
        templateUrl: 'pages/main.html',
        controller: 'mainController'
    })
    
    .when('/second', {
        templateUrl: 'pages/second.html',
        controller: 'secondController'
    })
    
    .when('/second/:num', {
        templateUrl: 'pages/second.html',
        controller: 'secondController'
    })
    
});

myApp.controller('mainController', ['$scope', '$log', function($scope, $log) {
    
        $scope.people = [{ 
            name :'Roshni Rana',
            address : '555 Main St.', 
            city :'Surat',
            state : 'Gujarat',
            zip : '394210'
        },{ 
            name :'nikita lad',
            address : '555 Main St.', 
            city :'Surat',
            state : 'Gujarat',
            zip : '394210'
        },{ 
            name :'jits Rana',
            address : '555 Main St.', 
            city :'Surat',
            state : 'Gujarat',
            zip : '394210'
        },{ 
            name :'Ragini Rana',
            address : '555 Main St.', 
            city :'Surat',
            state : 'Gujarat',
            zip : '394210'
        }]  

        $scope.formattedAddress = function(person){
            return person.address + ', '+ person.city +', ' + person.state  +' ' + person.zip;
        };
    
}]);

myApp.controller('secondController', ['$scope', '$log', '$routeParams', function($scope, $log, $routeParams) {
    
    
    
}]);

myApp.directive("searchResult", function() {
   return {
       restrict: 'AECM',
       templateUrl: 'directives/searchresult.html',
       replace: true,
    //    scope:{
    //        personName :"@",
    //        personAddress:"@"
    //    }
    scope:{
       personObject : "=",
       formattedAddressFunction :"&"
    },
        transclude:true
        // // compile :function(elem , attrs){
        // //     console.log('Compiling...');
        // //     console.log(elem.html());
            
        // //     return{
        // //         pre: function(scope,elements,attrs) {
        // //             console.log('Pre-linking..');
        // //             console.log(elements);
        // //         },
        //         link: function(scope,elements,attrs) {
        //             console.log('Linking..');
        //             console.log(scope);

        //             if(scope.personObject.name == 'doe'){
        //                 elements.removeAttr('class');
        //             }
        //             console.log(elements);
        //         }

        }
    
   
});
