var myApp = angular.module('myApp',[]);

myApp.controller('mainController',['$scope','$filter', function($scope , $filter){

    $scope.name = '';

    $scope.lowercasehandle = function(){
        return $filter('lowercase')($scope.name);
    }

    // $timeout(function() {
    //     $scope.name = 'EveryBody';
    //    },3000);

}]);