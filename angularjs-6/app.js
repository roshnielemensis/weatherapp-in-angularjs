var myApp = angular.module('myApp', ['ngRoute']);

myApp.config(function ($routeProvider) {
    
    $routeProvider
    
    .when('/', {
        templateUrl: 'pages/main.html',
        controller: 'mainController'
    })
    
    .when('/second/:num', {
        templateUrl: 'pages/second.html',
        controller: 'secondController'
    })
    
});

myApp.service('nameService', function() {

    var self = this;
    this.name = 'Roshni Rana';
    this.namelength = function(){
        return self.name.length;
    }
});

myApp.controller('mainController', ['$scope', '$log', 'nameService', function($scope, $log , nameService) {
    
    $scope.name = nameService.name;

    $scope.$watch('name',function() {
        nameService.name = $scope.name;
    });

    $log.log(nameService.name);
    $log.log(nameService.namelength());
    // $log.main = 'Property from main';
    // $log.log($scope);
    
}]);

myApp.controller('secondController', ['$scope','$log','$routeParams','nameService',function($scope,$log,$routeParams,nameService) {
    
    $scope.num = $routeParams.num || 1;

    $scope.name = nameService.name;

    // $log.second = 'Property from second';
    // $log.log($scope);
    
}]);
